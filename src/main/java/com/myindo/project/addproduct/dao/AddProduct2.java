/**
 * 
 */
package com.myindo.project.addproduct.dao;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Random;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import com.myindo.project.addproduct.connection.Connect;
import com.myindo.project.addproduct.model.RequestModel;


/**
 * @author Resti Pebriani
 *
 */
public class AddProduct2 {
	Logger log = Logger.getLogger("AddProduct2");
	String codeResponse;
	String path;
	 
//	private static final AtomicInteger count = new AtomicInteger(0); 
	
	public String addProduct(RequestModel rModel) throws Exception{
		Connection con=Connect.connection();
    	
		try {
//			//autoincreatment id
//	        uniqueID = count.incrementAndGet(); 
//	        String idString = "P000" +Integer.toString(uniqueID);
	        
//	        PreparedStatement po = con.prepareStatement("SELECT MAX(id_product) FROM product");
//	        ResultSet ro = po.executeQuery();
//	        while (ro.next()) {
//	        	hProduct.setId(ro.getString(1));
//			}
//	        System.out.println("ID : "+hProduct.getId());
//	        int re = Integer.parseInt(hProduct.getId());
//	        
//	        AtomicInteger count = new AtomicInteger(re); 
//	        uniqueID = count.incrementAndGet();
//	        String idString = uniqueID +Integer.toString(uniqueID);
//	        System.out.println("idString : "+idString);
	        
			//penamaan foto
			Random random =  new Random();
			int foto = random.nextInt(100000);
			String fotoString = Integer.toString(foto);
			
	        
	        //decode base64 to image and save to directory
			String base64String = rModel.getPicture();
			
			//convert base64 string to binary data
	        byte[] data = DatatypeConverter.parseBase64Binary(base64String);
	        path = "C:\\Users\\Resti Pebriani\\workspace\\picture\\PRDCT" +fotoString+".jpg";

	        File file = new File(path);
	        System.out.println("Picture disimpan di " +path);
	        
	        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
	            outputStream.write(data);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }        
	      
	        
//	        //randomUUID
//	        String uniq = UUID.randomUUID().toString();
//	        System.out.println("UUID : " + uniq);
	        
			if ( !rModel.getProductName().equals("") && !rModel.getStart_date().equals("") && !rModel.getEnd_date().equals("")
					&& !rModel.getPicture().equals("") && !rModel.getAmount().equals("") && !rModel.getStatus().equals("" ) 
					&& !rModel.getDescription().equals("")) {
				PreparedStatement ps = con
						.prepareStatement("INSERT INTO product (product_name, start_date, "
								+ "end_date, picture, amount, status, description) VALUES (?, ?::date, ?::date, ?, ?::int, ?, ?)");

				ps.setString(1, rModel.getProductName());
				ps.setString(2, rModel.getStart_date());
				ps.setString(3, rModel.getEnd_date());
				ps.setString(4, path);
				ps.setString(5, rModel.getAmount());
				ps.setString(6, rModel.getStatus());
				ps.setString(7, rModel.getDescription());

				ps.executeUpdate();
				ps.close();
				
				codeResponse = "0000";
				System.out.println("CodeResponse : " + codeResponse);
				log.info("Input data product berhasil");
				con.close();
			} else {
				codeResponse = "1111";
				System.out.println("CodeResponse : " + codeResponse);
				log.info("Data tidak boleh kosong");
			}
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			System.out.println("CodeResponse : " + codeResponse);
			log.info("Input data product gagal");
			log.info(e.getMessage());
		}
		return codeResponse;
	}
}
